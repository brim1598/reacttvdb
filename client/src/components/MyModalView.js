import React from "react";

import { Row, Container, Col, Modal } from "react-bootstrap";

const MyModalView = props => {
  const { onHide, infoObj } = props;

  return (
    <Modal {...props} aria-labelledby="contained-modal-title-vcenter">
      <Modal.Header
        closeButton
        onClick={onHide}
        style={{ background: "whitesmoke" }}
      >
        <Modal.Title id="contained-modal-title-vcenter">
          {infoObj.title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="detailCard">
        <Container>
          <Row>
            <Col>
              <img
                src={infoObj.image}
                alt={infoObj.title}
                style={{ margin: "auto", display: "block" }}
              ></img>
            </Col>
          </Row>
          <hr></hr>
          <Row>
            <Col>
              <span className="modaloverview">Overview: </span>
              {infoObj.overview}
            </Col>
          </Row>
          <hr></hr>

          <Row>
            <Col>
              <span className="modaloverview">Director: </span>
              {infoObj.director}
            </Col>
          </Row>
        </Container>
      </Modal.Body>
    </Modal>
  );
};

export default MyModalView;

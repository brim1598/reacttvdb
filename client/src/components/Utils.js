export const getImage = obj => {
  if (obj) {
    return "http://thetvdb.com" + obj;
  }
  return require("../noimage.png");
};

export const getBannersImage = obj => {
  if (obj) {
    return "http://thetvdb.com/banners/" + obj;
  }
  return require("../noimage.png");
};

export const getNword = str => {
  let overview = "";
  if (str) {
    overview = str
      .split(/\s+/)
      .slice(0, 40)
      .join(" ");
    if (str.length > overview.length) {
      overview += " ...";
    }
  }
  return overview;
};

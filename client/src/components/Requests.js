import axios from "axios";

const requestFunction = async (
  method = "",
  url = "",
  headersObj = {},
  params = {}
) => {
  let headers = {
    "Target-URL": "https://api.thetvdb.com"
  };
  Object.assign(headers, headersObj);
  return await axios({
    method: method,
    url: url,
    params: params,
    headers: headers
  })
    .then(res => {
      return res.data;
    })
    .catch(res => {
      return res;
    });
};

export const getJWTtoken = () => {
  return requestFunction("POST", "/login");
};

export const searchSerie = stringValue => {
  let params = {
    name: stringValue
  };
  let headers = {
    Authorization: "Bearer " + localStorage.getItem("token")
  };
  return requestFunction("GET", "/search/series", headers, params);
};

export const getSerieById = id => {
  let headers = {
    Authorization: "Bearer " + localStorage.getItem("token")
  };
  return requestFunction("GET", "/series/" + id, headers);
};

export const getSummaryOfTheEpisodes = id => {
  let headers = {
    Authorization: "Bearer " + localStorage.getItem("token")
  };
  return requestFunction("GET", "/series/" + id + "/episodes/summary", headers);
};

export const getEpisodesById = (id, page) => {
  let params = {
    page: page
  };
  let headers = {
    Authorization: "Bearer " + localStorage.getItem("token")
  };
  return requestFunction("GET", "/series/" + id + "/episodes", headers, params);
};

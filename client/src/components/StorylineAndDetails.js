import React from "react";
import { Card, Row, Col } from "react-bootstrap";

const StorylineAndDetails = props => {
  let key = 0;
  function getStars() {
    let stars = [];
    if (props.siteRating) {
      var whole = parseInt(props.siteRating);
      var rest = props.siteRating - whole;
      while (whole > 0) {
        stars.push(
          <span key={key} className="spanclassstar">
            &#9733;
          </span>
        );
        key++;
        whole--;
      }
      if (rest > 0.5) {
        stars.push(
          <span key={key} className="spanclassstar">
            &#9733;
          </span>
        );
        key++;
      }
    }

    while (stars.length < 10) {
      stars.push(
        <span key={key} className="spanclassstar">
          &#9734;
        </span>
      );
      key++;
    }

    stars.push(
      <span key={key} className="spanclassstarRate">
        ({props.siteRating}/10)
      </span>
    );

    return stars;
  }

  let stars = getStars();

  return (
    <div>
      <Row>
        <Col>
          <Card className="card-body-own">
            <Card.Title
              style={{
                padding: "15px",
                fontSize: "1.7rem",
                marginBottom: "auto"
              }}
            >
              Storyline
            </Card.Title>
            <Card.Body className="borderTopclass">
              <Card.Text>{props.overview ? props.overview : "-"}</Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      <br></br>
      <Row>
        <Col>
          <Card className="card-body-own">
            <Card.Title
              style={{
                padding: "15px",
                fontSize: "1.7rem",
                marginBottom: "auto"
              }}
            >
              Details
            </Card.Title>
            <Card.Body className="borderTopclass">
              <Card.Text>
                <span>Status: </span>
                {props.status}
                <br></br>
                <span>Airs day of week: </span>
                {props.airsDayOfWeek ? props.airsDayOfWeek : "-"}
                <br></br>
                <span>Airs time: </span>
                {props.airsTime ? props.airsTime : "-"}
                <br></br>
                <span>Genre: </span>
                {props.genre ? props.genre.join(", ") : "-"}
                <br></br>
                <span>Site rating: </span>
                {props.siteRating ? stars : "-"}
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>{" "}
    </div>
  );
};

export default StorylineAndDetails;

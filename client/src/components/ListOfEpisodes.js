import React from "react";
import { Row, Col, Card, Button, Dropdown } from "react-bootstrap";

import { getBannersImage } from "./Utils";

const ListOfEpisodes = props => {
  function getEpisodeDetailsView() {
    let episodesDetails = [];

    props.episodesPerSeason.map((value, index, array) => {
      let name = value.episodeName;
      let firstAired = value.firstAired;
      let image = getBannersImage(value.filename);
      let siteRating = value.siteRating;

      return episodesDetails.push(
        <div key={index}>
          <Card className="borderFullclass">
            <Row>
              <Col xs={5}>
                <Card key={index} className="noborder">
                  <Card.Img src={image} alt={name}></Card.Img>
                </Card>
              </Col>
              <Col>
                <Card className="noborder detailCard">
                  <Card.Body>
                    <h5 style={{ fontSize: "24px" }}>{name}</h5>
                    <br></br>
                    <br></br>
                    <div style={{ fontSize: "18px" }}>Rating: {siteRating}/10 </div>
                    <br></br>
                    <div style={{ fontSize: "18px" }}>
                      Release: {firstAired}
                    </div>
                    <br></br>
                    <Button
                      variant="info"
                      onClick={e =>
                        props.setEpisodeID(e.target.firstChild.value)
                      }
                      style={{ float: "right" }}
                    >
                      <input
                        type="hidden"
                        id="episodeID"
                        name="episodeID"
                        value={value.id}
                      ></input>
                      More details
                    </Button>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </Card>
          <hr></hr>
        </div>
      );
    });

    return episodesDetails;
  }

  function getDropdownItems() {
    let items = [];
    if (props.summary.airedSeasons) {
      var val = 0;
      for (const [index, value] of props.summary.airedSeasons
        .map(Number) // convert to Number
        .sort((a, b) => a - b)
        .entries()) {
        if (value !== 0) {
          val = value;
          items.push(
            <Dropdown.Item
              key={index}
              onClick={e => props.setSeasonID(parseInt(e.target.text))}
            >
              {val}
            </Dropdown.Item>
          );
        }
      }
    }
    return items;
  }

  let dropItems = getDropdownItems();
  let episodesDetails = getEpisodeDetailsView();

  return (
    <div>
      <Row>
        <Col>
          <Card className="inheritCardBody card-body-own">
            <Card.Title
              style={{
                padding: "15px",
                fontSize: "1.7rem",
                marginBottom: "auto"
              }}
            >
              <div>
                Episode List
                <br />
                <br />
                <Dropdown>
                  <Dropdown.Toggle variant="info" id="dropdown-basic">
                    Select Season
                  </Dropdown.Toggle>
                  <Dropdown.Menu>{dropItems}</Dropdown.Menu>
                </Dropdown>
              </div>
            </Card.Title>
            <Card.Body className="borderCardBody">{episodesDetails}</Card.Body>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default ListOfEpisodes;

import React from "react";
import { Card, CardColumns } from "react-bootstrap";
import queryString from "query-string";
import { searchSerie, getJWTtoken } from "./Requests";
import Search from "./Search";
import { getImage, getNword } from "./Utils";
import ReactDOM from "react-dom";
import ErrorNotFound from "./ErrorNotFound";

class MainPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      series: [],
      exists: true,
      serieName: ""
    };
    this.moveToDetailPage = this.moveToDetailPage.bind(this);
  }

  componentDidMount() {
    getJWTtoken()
      .then(res => {
        localStorage.setItem("token", res.token);
      })
      .catch(err => {
        console.log(err);
      });

    if (queryString.parse(this.props.location.search).q) {
      this.setSeriaName(queryString.parse(this.props.location.search).q);
      this.searchByString(queryString.parse(this.props.location.search).q);
    }
  }

  componentWillUnmount() {
    this.setState({ exists: true });
  }

  setSeriaName = str => {
    this.setState({
      serieName: str
    });
  };

  searchByString = stringValue => {
    if (stringValue !== "") {
      searchSerie(stringValue)
        .then(res => {
          if (res.data) {
            this.setState({ series: res.data });
            this.setState({ exists: true });
            this.props.history.push("/search?q=" + stringValue);
          } else {
            this.setState({ exists: false });
            ReactDOM.render(
              <ErrorNotFound />,
              document.getElementsByName("notFoundPage")[0]
            );
          }
        })
        .catch(err => {
          console.log(err);
          this.setState({ series: [] });
        });
    }
  };

  moveToDetailPage = e => {
    let linkTo = "/detail/" + e.target.firstChild.value;
    this.props.history.push(linkTo);
  };

  getCards = () => {
    let Cards = [];
    if (this.state.series) {
      Cards = [];
      const cards = this.state.series;
      cards.map((value, index, array) => {
        let srcimg = getImage(value.banner);
        let overview = getNword(value.overview);

        return Cards.push(
          <Card key={index}>
            <Card.Img variant="top" src={srcimg} alt={value.seriesName} />
            <Card.Body className="card-body-own">
              <Card.Title>{value.seriesName}</Card.Title>
              <hr></hr>
              <Card.Text>{overview}</Card.Text>
              <hr></hr>
              <div style={{ float: "left", fontSize: "12px" }}>
                Network: {value.network ? value.network : "-"}
              </div>
              <br></br>
              <div style={{ float: "left", fontSize: "12px" }}>
                Release: {value.firstAired ? value.firstAired : "-"}
              </div>
            </Card.Body>
            <Card.Footer
              className="card-footer-bg"
              onClick={e => this.moveToDetailPage(e)}
            >
              <input type="hidden" name="id" value={value.id}></input>
              More detail
            </Card.Footer>
          </Card>
        );
      });
    }
    return Cards;
  };

  render() {
    let Cards = this.getCards();

    let moreThan = this.state.series.length >= 100 ? "more than" : "";

    return (
      <div>
        {!this.state.exists && <div name="notFoundPage" />}
        <br></br>
        <Search
          onTitleChange={this.searchByString}
          seriename={this.state.serieName}
          setSeriaName={this.setSeriaName}
        />
        <br></br>
        {this.state.exists && (
          <div className="resultsCount">
            <br />
            <span>
              Movie results: {moreThan} {this.state.series.length}
            </span>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
          </div>
        )}
        {this.state.exists && <CardColumns>{Cards}</CardColumns>}
        <br />
        <br />
      </div>
    );
  }
}

export default MainPage;

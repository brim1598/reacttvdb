import React from "react";
import ReactDOM from "react-dom";
import { Row, Container, Col } from "react-bootstrap";
import {
  getSerieById,
  searchSerie,
  getEpisodesById,
  getSummaryOfTheEpisodes
} from "./Requests";
import ErrorNotFound from "./ErrorNotFound";
import Search from "./Search";
import { getImage,getBannersImage } from "./Utils";
import StorylineAndDetails from "./StorylineAndDetails";
import ListOfEpisodes from "./ListOfEpisodes";
import queryString from "query-string";
import MydModalView from "./MyModalView";

class DetailPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      details: [],
      summary: [],
      episodesPerSeason: [],
      pages: 0,
      seasonID: 1,
      modalShow: false,
      episodeIdForModal: 0,
      exists: true,
      serieName: ""
    };
  }

  componentDidMount() {
    this.searchById(this.props.match.params.id);
  }

  componentWillUnmount() {
    this.setState({ exists: true });
    this.setState({ modalShow: false });
  }

  setSeriaName = str => {
    this.setState({
      serieName: str
    });
  };

  searchById = id => {
    if (id !== "") {
      getSerieById(id)
        .then(res => {
          if (res.data) {
            this.setState({ details: res.data });
            this.getSummary(this.props.match.params.id);
            this.setState({ exists: true });
            this.setSeriaName(this.state.details.seriesName)
          } else {
            this.setState({ exists: false });
            ReactDOM.render(
              <ErrorNotFound />,
              document.getElementsByName("notFoundPage")[0]
            );
          }
        })
        .catch(err => {
          console.log(err);
          this.setState({ details: [] });
        });
    }
  };

  getSummary = id => {
    if (id !== "") {
      getSummaryOfTheEpisodes(id)
        .then(res => {
          if (res.data) {
            this.setState({ summary: res.data });
            this.setState({
              pages: parseInt(this.state.summary.airedEpisodes / 100 + 1)
            });
            if (queryString.parse(this.props.location.search).seasonID) {
              this.setState({
                seasonID: parseInt(
                  queryString.parse(this.props.location.search).seasonID
                )
              });
            }
            this.getEpisodes(this.props.match.params.id, this.state.seasonID);
          }
        })
        .catch(err => {
          console.log(err);
          this.setState({ summary: [] });
        });
    }
  };

  getAllEpisodesBySeasonID = res => {
    let episodes = [];
    res.forEach((value, index) => {
      let result = value.data.filter(
        episode => episode.airedSeason === parseInt(this.state.seasonID)
      );
      if (result.length > 0) {
        episodes = episodes.concat(result);
      }
    });
    return episodes;
  };

  getEpisodes = (id, seasonID) => {
    if (id !== "" && seasonID !== "" && this.state.pages > 0) {
      let pages = Array.from(Array(this.state.pages), (x, index) => index + 1); // [1...n]
      var actions = pages.map((value, index, array) => {
        return getEpisodesById(id, value);
      });

      var results = Promise.all(actions);

      results.then(res => {
        let episodes = this.getAllEpisodesBySeasonID(res);
        this.setState({ episodesPerSeason: episodes });
        if (this.state.episodesPerSeason.length > 0) this.sortEpisodes();
      });
    }
  };

  searchByString = stringValue => {
    if (stringValue !== "") {
      searchSerie(stringValue)
        .then(res => {
          if (res.data) {
            this.setState({ series: res.data });
            this.setState({ exists: true });
            this.props.history.push("/search?q=" + stringValue);
          } else {
            this.setState({ exists: false });
            ReactDOM.render(
              <ErrorNotFound />,
              document.getElementsByName("notFoundPage")[0]
            );
          }
        })
        .catch(err => {
          console.log(err);
          this.setState({ series: [] });
        });
    }
  };

  sortEpisodes = () => {
    if (this.state.episodesPerSeason) {
      this.state.episodesPerSeason.sort(function(a, b) {
        return parseInt(a.airedEpisodeNumber) - parseInt(b.airedEpisodeNumber);
      });
      this.setState({ episodeIdForModal: this.state.episodesPerSeason[0].id });
    }
  };

  setSeasonID = newId => {
    this.setState({ seasonID: parseInt(newId) });
    this.props.history.push({ search: "seasonID=" + parseInt(newId) });
    this.getEpisodes(this.props.match.params.id, this.state.seasonID);
  };

  setModalShow = value => {
    this.setState({ modalShow: value });
  };

  setEpisodeID = id => {
    this.setState({ episodeIdForModal: parseInt(id) });
    this.setModaltInfo();
    this.setModalShow(true);
  };

  setModaltInfo = () => {
    let modalValues = {};
    if (this.state.episodesPerSeason) {
      let obj = this.state.episodesPerSeason.find(element => {
        return parseInt(element.id) === this.state.episodeIdForModal;
      });
      if (obj) {
        modalValues.title = obj.episodeName;
        modalValues.overview = obj.overview;
        modalValues.image = getImage(obj.filename);
        modalValues.director = obj.director;
      }
    }
    return modalValues;
  };

  render() {
    let srcimg = getBannersImage(this.state.details.banner);
    let modalInfo = this.setModaltInfo();

    return (
      <div className="container">
        {!this.state.exists && <div name="notFoundPage" />}
        <Search
          onTitleChange={this.searchByString}
          seriename={this.state.serieName}
          setSeriaName={this.setSeriaName}
        />
        <br></br>
        <br></br>
        {this.state.exists && (
          <Container>
            <Row>
              <Col className="seriesImg">
                <img src={srcimg} alt={this.state.details.seriesName}></img>
              </Col>
            </Row>
            <br></br>
            <br></br>
            <Row>
              <Col md="auto">
                <h1 className="serieTitle">{this.state.details.seriesName}</h1>
              </Col>
            </Row>
            <hr></hr>
            <StorylineAndDetails
              overview={this.state.details.overview}
              status={this.state.details.status}
              airsDayOfWeek={this.state.details.airsDayOfWeek}
              airsTime={this.state.details.airsTime}
              genre={this.state.details.genre}
              siteRating={this.state.details.siteRating}
            />
            <br></br>
            <ListOfEpisodes
              episodesPerSeason={this.state.episodesPerSeason}
              summary={this.state.summary}
              setSeasonID={this.setSeasonID}
              setEpisodeID={this.setEpisodeID}
            />

            <MydModalView
              show={this.state.modalShow}
              onHide={() => this.setModalShow(false)}
              infoObj={modalInfo}
            />
          </Container>
        )}
      </div>
    );
  }
}

export default DetailPage;

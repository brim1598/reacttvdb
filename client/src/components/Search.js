import React from "react";

const Search = props => {
  function _handleKeyDown(e) {
    if (e.key === "Enter") {
      props.onTitleChange(props.seriename);
    }
  }

  return (
    <div className="input-group input-group-lg" style={{ width: "auto" }}>
      <input
        type={props.seriename}
        className="form-control"
        aria-label="Sizing example input"
        aria-describedby="inputGroup-sizing-lg"
        onKeyDown={_handleKeyDown}
        placeholder="Enter the serie name"
        value={props.seriename}
        onChange={e => props.setSeriaName(e.target.value)}
      ></input>
      <span>&nbsp;&nbsp;</span>
      <button
        className="btn btn-secondary btn-lg"
        type="submit"
        onClick={e => props.onTitleChange(props.seriename)}
      >
        Search
      </button>
    </div>
  );
};

export default Search;

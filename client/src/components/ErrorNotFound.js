import React from "react";

const ErrorNotFound = props => {
  return (
    <div>
      <br />
      <br />
      <br />
      <h1 className="serieTitle">Oops! That page can’t be found.</h1>
      <br />
      <br />
      <br />
    </div>
  );
};

export default ErrorNotFound;

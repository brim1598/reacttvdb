import React from "react";
import "../App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Header from "./Header";
import MainPage from "./MainPage";
import ErrorNotFound from "./ErrorNotFound";
import DetailPage from "./DetailPage";

class App extends React.Component {
  render() {
    return (
      <Router>
        <div className="App container">
          <Header text="TV Show Database" />
          <br></br>
          <div>
            <Switch>
              <Route exact path="/" component={MainPage} />
              <Route path="/search" component={MainPage} />
              <Route path="/search?q=:searchValue" component={MainPage} />
              <Route
                path="/detail/:id?seasonID=:seasonID"
                component={DetailPage}
              />
              <Route path="/detail/:id" component={DetailPage} />
              <Route component={ErrorNotFound} />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
